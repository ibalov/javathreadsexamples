/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package clases;

/**
 *
 * @author Maik
 */
public class App {

    public static void main(String[] args) {
        int numberThreads = 3;
        
        //loop of threads
        for (int i = 0; i < numberThreads; i++) {
            new Thread(new myThread("Thread-"+(i+1))).start();
        }
        
    }
}
