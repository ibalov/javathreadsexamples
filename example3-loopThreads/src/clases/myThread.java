/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package clases;

import java.time.LocalTime;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Maik
 */
public class myThread implements Runnable {
    //ATRIBUTES
    //attributes – only if needed

    //CONSTRUCTOR – only if needed a non-default
    myThread(String name){
        Thread.currentThread().setName(name);
    }
    //TASK METHOD
    @Override
    public void run() {
        for (int i = 0; i < 5; i++) {
            System.out.printf("\n%s, %s, %s\n",
                    Thread.currentThread().getName(),
                    Thread.currentThread().getClass().toString(),
                    LocalTime.now().toString());
            

            try {
                Thread.sleep(2000);
                
            } catch (InterruptedException ex) {
                ex.printStackTrace();
            }
        }
    }

}
