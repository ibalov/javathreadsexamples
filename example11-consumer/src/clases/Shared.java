/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package clases;

/**
 *
 * @author Maik
 */
public class Shared {
    //Flag
    boolean consumerAllowed = false;
    //Shared data
    private char data;

    //Constructors
    Shared() {
        this.data = '0';
    }

    Shared(char data) {
        this.data = data;
    }

    //Method for getting the data
    synchronized char get() throws InterruptedException {
        //Fixin the race condition
        while (!consumerAllowed) {
            wait();
        }
        System.out.println("Got: " + data);
        //Return flag to false
        consumerAllowed = false;
        //Notify the other thread
        notify();
        return data;
    }

    //Method for putting data
    synchronized void put(char data) throws InterruptedException {
        //Fixing the race condition
        while (consumerAllowed) {
            wait();
        }
        this.data = data;
        System.out.println("Put: " + data);
        consumerAllowed = true;
        notify();
    }
    
    
}
