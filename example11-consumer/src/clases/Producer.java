/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package clases;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Maik
 */
public class Producer implements Runnable {

    //Shared
    Shared s = null;
    //Data
    char[] data = {'H', 'e', 'l', 'l', 'o', 'W', 'o', 'r', 'l', 'd'};

    //Constructor
    Producer(Shared s) {
        this.s = s;
    }

    //Task
    @Override
    public void run() {
        for (int i = 0; i < data.length; i++) {
            try {
                s.put(data[i]);
                Thread.sleep(200);
            } catch (InterruptedException ex) {
                Logger.getLogger(Producer.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}
