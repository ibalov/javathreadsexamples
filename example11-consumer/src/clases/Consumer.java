/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package clases;
import java.util.logging.Level;
import java.util.logging.Logger;
/**
 *
 * @author Maik
 */
public class Consumer implements Runnable {
    //Shared
    Shared s = null;
    //Constructor
    Consumer(Shared s){
        this.s = s;
    }
    @Override
    public void run() {
        for (int i = 0; i < 20; i++){
            try {
                s.get();
                Thread.sleep(500);
            } catch (InterruptedException ex) {
                Logger.getLogger(Consumer.class.getName()).log(Level.SEVERE, null, ex);
            }

        }
    }
}
