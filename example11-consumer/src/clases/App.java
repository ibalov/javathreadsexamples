/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package clases;

/**
 *
 * @author Maik
 */
public class App {

    public static void main(String[] args) {
        //Shared object
        Shared s = new Shared();
        //Threads
        Thread p = new Thread(new Producer(s), "Producer");
        Thread c = new Thread(new Consumer(s), "Consumer");
        //Starting Threads
        p.start();
        c.start();
        
    }
}
