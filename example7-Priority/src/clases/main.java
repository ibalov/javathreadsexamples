/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package clases;

/**
 *
 * @author Maik
 */
public class main {

    public static void main(String[] args) {
        //Main thread priority
        Thread mt = Thread.currentThread();
        System.out.println("Main thread priority: "
                + mt.getPriority());
        //Threads priority   
        Thread t = null;
        for (int i = 0; i < 5; i++) {
            t = new Thread(new myThread(), "Avenger-" + i);
            t.start();
            System.out.println("Thread " + t.getName()
                    + " has priority " + t.getPriority());
            if ((i % 2) != 0) {
                t.setPriority(10);
            } else {
                t.setPriority(Thread.MIN_PRIORITY);
            }

            System.out.println("Thread " + t.getName()
                    + " has priority " + t.getPriority());
        }
    }
}