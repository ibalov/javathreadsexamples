/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package clases;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Maik
 */
public class myThread implements Runnable {

    //THREAD PRIORITY
    @Override
    public void run() {
        for (int i = 0; i < 5; i++) {
            System.out.printf("\nIt's %s, with priority %d\n",
                    Thread.currentThread().getName(),
                    Thread.currentThread().getPriority());
            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                e.getCause().toString();
            }
        }

    }
}
