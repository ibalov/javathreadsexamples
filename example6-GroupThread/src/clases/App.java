/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package clases;

/**
 *
 * @author Maik
 */
public class App {
    public static void main(String[] args) {
        //ThreadGroup
        ThreadGroup tg = new ThreadGroup("Avengers");
        //Threads
        for (int i = 0; i < 10; i++) {
            Thread t = new Thread(tg, new myThread(), "Avenger-"+i);
            t.start();
            //prints
            System.out.println(tg.activeCount());
            System.out.println(tg.toString());
        }
        //getting Threads
        Thread[] avengers = new Thread[tg.activeCount()];
        int i = tg.enumerate(avengers);
        for (Thread t: avengers) {
            System.out.println("I've found "+t.getName());
        }
        
        //ends
        System.out.println("That's all folks");
    }
}
