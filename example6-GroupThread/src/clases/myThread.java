/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package clases;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Maik
 */
public class myThread implements Runnable{

    @Override
    public void run() {
        try {
            //Sleeps for 5s
            Thread.sleep(5000);
        } catch (InterruptedException ex) {
            Logger.getLogger(myThread.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.out.printf("\nHey, it's me: %s, from %s\n",
                        Thread.currentThread().getName(),
                        Thread.currentThread().getThreadGroup());
    }
    
}
