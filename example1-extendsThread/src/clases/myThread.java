/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package clases;

import java.time.LocalTime;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Maik
 */
public class myThread extends Thread{
    
    myThread (String name){
        super(name);
    }
    
    @Override
    public void run() {
        for (int i = 0; i < 5; i++) {
            System.out.printf("\n%s, %s, %s\n",
                            this.getName(),
                            this.getClass().toString(),
                            LocalTime.now().toString());
            try {
                this.sleep(2000);
            } catch (InterruptedException ex) {
                ex.printStackTrace();
            }
        }
        
    }
}
