/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package clases;

/**
 *
 * @author Maik
 */
public class App {

    public static void main(String[] args) {
        Thread t1 = new Thread(new myThread());
        t1.setName("Maik");
        t1.start();

        Thread t2 = new Thread(new myThread());
        t2.setName("Sara");
        t2.start();

        Thread t3 = new Thread(new myThread());
        t3.setName("Juan");
        t3.start();
    }
}
