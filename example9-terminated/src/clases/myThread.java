/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package clases;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Maik
 */
public class myThread implements Runnable{
    //FLAGS
    private boolean isSuspended = false;
    private boolean isTerminated = false;
    //TASKS
    @Override
    public void run() {
        //Task and termination condition
        for(int i = 0; i < 5 && !isTerminated; i++){          
            System.out.printf("\nThread %s in my %dth iteration.\n",
                                    Thread.currentThread().getName(),
                                    i + 1);
            try {
                Thread.sleep(5000);
                } catch (InterruptedException ex) {
                    Logger.getLogger(myThread.class.getName()).log(Level.SEVERE, null, ex);
                }
            //Suspension
            synchronized(this) {
                while(isSuspended) {
                    System.out.println(Thread.currentThread().getName() + "got paused!");
                    try {
                        this.wait();
                        } catch (InterruptedException ex) {
                            Logger.getLogger(myThread.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    System.out.println(Thread.currentThread().getName() + " got resumed!");
                }
                
            }
        }
        //Normal termination
        if (!isTerminated)
            System.out.println("Great! No one terminated me! " 
                            + Thread.currentThread().getName());
        else
            System.out.println("Oh, I was terminated... " 
                            + Thread.currentThread().getName());
    }             
    //METHODS
    //Suspending
    public synchronized void setSuspended() {
        this.isSuspended = true;       
    }
    public synchronized void setResumed() {
        this.isSuspended = false;
        this.notify();        
    } 
    //Terminating
    public synchronized void terminate(){
        this.isTerminated = true;
    }
}
