/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package pkgsynchronized;

/**
 *
 * @author Maik
 */
public class CallMe {
   //method
    void call(String msg) throws InterruptedException{
        System.out.print("["+msg);
        Thread.sleep(1000);
        System.out.print("Interrupted");
        System.out.println("]");
    }
}
