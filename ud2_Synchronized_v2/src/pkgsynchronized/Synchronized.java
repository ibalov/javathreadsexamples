/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package pkgsynchronized;

/**
 *
 * @author Maik
 */
public class Synchronized {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws InterruptedException {
        //Shared object
        CallMe target = new CallMe();
        //threads
        Thread t1 = new Thread(new Caller("Hello",target),"caller-1");
        Thread t2 = new Thread(new Caller("Synchronized",target),"caller-2");
        Thread t3 = new Thread(new Caller("World",target),"caller-3");
        
       //starting threads
       t1.start();
       t2.start();
       t3.start();
       //waiting
       t1.join();
       t2.join();
       t3.join();
    }
    
}
