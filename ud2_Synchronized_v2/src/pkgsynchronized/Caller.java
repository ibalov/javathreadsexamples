/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package pkgsynchronized;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Maik
 */
public class Caller implements Runnable {

    private String msg;
    private CallMe target;

    //constructor
    Caller(String msg, CallMe target) {
        this.msg = msg;
        this.target = target;
    }

    //Run
    @Override
    public void run() {
        //Synchronized block over the target
        synchronized (target) {
            try {
                target.call(msg);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

}
