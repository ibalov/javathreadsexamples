/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package clases;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Maik
 */
public class myThread implements Runnable {

    //suspend flag
    private boolean isSuspended;

    @Override
    public void run() {
        for (int i = 0; i < 5 && !Thread.currentThread().isInterrupted(); i++) {
            System.out.printf("\nIt's %s, with number iteration %d\n",
                    Thread.currentThread().getName(), i + 1);
            try {
                Thread.sleep(2000);
            } catch (InterruptedException ex) {
                Logger.getLogger(myThread.class.getName()).log(Level.SEVERE, null, ex);
            }

            //Suspension
            synchronized (this) {
                while (isSuspended) {
                    System.out.println(Thread.currentThread().getName() + "got paused!");
                    try {
                        this.wait();
                    } catch (InterruptedException ex) {
                        Logger.getLogger(myThread.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    System.out.println(Thread.currentThread().getName() + " got resumed!");
                }

            }

        }
        //Normal termination
        System.out.println("Great! No one terminated me! "
                + Thread.currentThread().getName());

    }

    //Methods
    public synchronized void setSuspended() {
        this.isSuspended = true;
    }

    public synchronized void setResumed() {
        this.isSuspended = false;
        this.notify();
    }

}
