/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package clases;

/**
 *
 * @author Maik
 */
public class App {
    public static void main(String[] args) throws InterruptedException {
         //Threads
        myThread hmt = new myThread();
        Thread h = new Thread(hmt, "Hanibal");
        h.start();
        
        myThread bamt = new myThread();
        Thread ba = new Thread(bamt, "Bad Attitude");
        ba.start();
        
        myThread pmt = new myThread();
        Thread p = new Thread (pmt, "Phoenix");
        p.start();
        
        //Managing states
        Thread.sleep(2000);
        pmt.setSuspended();     //Suspending Phoenix
        Thread.sleep(3000);
        hmt.setSuspended();     //suspending Hanibal
        Thread.sleep(1000);
        h.interrupt();
        ba.interrupt();
        Thread.sleep(8000);
        hmt.setResumed();
        pmt.setResumed();      
    }
}
